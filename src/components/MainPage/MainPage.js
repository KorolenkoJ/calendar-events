import Calendar from '../Calendar/Calendar.vue'
import Pagination from '../Pagination/Pagination.vue'
import Event from '../Event/Event.vue'
import QuickEvent from '../QuickEvent/QuickEvent.vue'
import SearchEvent from '../SearchEvent/SearchEvent.vue'

export default {
  components: {
    Calendar,
    Pagination,
    Event,
    QuickEvent,
    SearchEvent
  },

  data: () => ({
    curYear: new Date().getFullYear(),
    curMonth: new Date().getMonth(),
    curEl: null,
    time: 0,
    selectedEventTime: 0,
    eventsList: [],
    isShowEventModal: false
  }),

  created () {
    this.getEventsList()
  },

  methods: {
    getEventsList () {
      if (localStorage.getItem('events')) {
        this.eventsList = [...JSON.parse(localStorage.getItem('events'))]
      }
    },

    setNewMonth (k) {
      const d = new Date(this.curYear, this.curMonth + k, 1)
      this.curMonth = d.getMonth()
      this.curYear = d.getFullYear()
    },

    changeStatusEventModal (val) {
      this.isShowEventModal = val
    },

    showEventModal (obj) {
      this.curEl = obj.el
      this.time = obj.time
      this.isShowEventModal = obj.isShowEventModal
    },

    setEvent (obj, isNew) {
      if (isNew) {
        this.eventsList.push(obj)
      } else {
        this.eventsList.find((item) => {
          if (item.time === obj.time) {
            item.name = obj.name
            item.members = obj.members
            item.desc = obj.desc
          }
        })
      }

      localStorage.setItem('events', JSON.stringify(this.eventsList))
    },

    removeEvent (id) {
      this.eventsList.splice(id, 1)
      localStorage.setItem('events', JSON.stringify(this.eventsList))
    },

    goToEvent (item) {
      const event = this.eventsList.find((e) => e.time === item.time)
      if (event) {
        this.selectedEventTime = event.time
        const d = new Date(this.selectedEventTime)
        this.curMonth = d.getMonth()
        this.curYear = d.getFullYear()
      }
    },

    resetSearchRes () {
      this.selectedEventTime = 0
    }

  }
}
