export default {
  props: {
    curMonth: Number,
    curYear: Number,
    eventsList: Array,
    selectedEventTime: Number,
    isShowEvent: Boolean
  },

  data: () => ({
    weekDaysLong: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
    weekDaysShort: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    firstMonthDay: 1,
    week: 7,
    curEl: null,
    timeCode: null,
    ifWindowSmall: false
  }),

  created () {
    window.addEventListener('resize', () => {
      this.ifWindowSmall = (window.innerWidth > 1000)
      this.isShowEventModal = false
    })
  },

  watch: {
    isShowEvent (val) {
      if (!val) this.curEl.classList.remove('calendar-table_active')
    },

    selectedEventTime (val) {
      if (val > 0) {
        this.$nextTick(() => {
          this.showSelectedEvent()
        })
      }
    }
  },

  computed: {
    getCurCalendar () {
      let weekDay = 0
      const calendar = []
      let firstDay = new Date(this.curYear, this.curMonth, 1).getDay()
      const daysInMonth = new Date(this.curYear, this.curMonth + 1, 0).getDate()
      let lastDay = new Date(this.curYear, this.curMonth, daysInMonth).getDay()
      const prevMonthDay = new Date(this.curYear, this.curMonth, 0).getDate()
      let todayClass = ''
      let eventClass = ''
      let obj = {}

      calendar[weekDay] = []
      firstDay = firstDay === 0 ? 7 : firstDay
      lastDay = lastDay === 0 ? 7 : lastDay

      for (let i = 1; i <= daysInMonth; i++) {
        obj = {
          day: i,
          date: this.getStringDate(i),
          classes: 'calendar-table_cur'
        }

        this.eventsList.find((item) => {
          if (item.time === this.getTimeMls(i)) {
            obj.event = {
              name: item.name,
              members: item.members.join(', ')
            }
          }
        })

        if (new Date(this.curYear, this.curMonth, i).getDay() === this.firstMonthDay) {
          weekDay++
          calendar[weekDay] = []
        }

        todayClass = (new Date().getDate() === i && this.isRelevantDate()) && 'calendar-table_cur-day'

        eventClass = obj.event && 'calendar-table__event-day'

        if (todayClass) obj.classes += ` ${todayClass}`
        if (eventClass) obj.classes += ` ${eventClass}`

        calendar[weekDay].push(obj)
      }

      if (firstDay !== this.firstMonthDay) { // if prev months day
        for (let i = 1; i < firstDay; i++) {
          calendar[0].splice(0, 0, { day: prevMonthDay - i + 1, classes: 'calendar-table_inactive' })
        }
      }

      if (lastDay !== this.week) { // if next months day
        let day = 1
        for (let i = this.week; i > lastDay; i--) {
          calendar[weekDay].push({ day: day, classes: 'calendar-table_inactive' })
          day++
        }
      }

      if (!calendar[0].length) {
        calendar.splice(0, 1)
      }

      return calendar
    },

    getDayName () {
      return this.ifWindowSmall ? this.weekDaysLong : this.weekDaysShort
    }

  },

  methods: {
    getStringDate (day) {
      const m = (this.curMonth + 1) < 10 ? `0${this.curMonth + 1}` : `${this.curMonth + 1}`
      const d = day < 10 ? `0${day}` : `${day}`
      return `${this.curYear}-${m}-${d}`
    },

    getTimeMls (val) {
      const str = typeof val === 'number' ? this.getStringDate(val) : val
      return Date.parse(`${str}T00:00:00`)
    },

    showEvenModal (e = null, el = null) {
      if (this.curEl) {
        this.curEl.classList.remove('calendar-table_active')
      }

      if (e) this.curEl = e.target.closest('.calendar-table__item')
      if (el) this.curEl = el

      this.timeCode = this.getTimeMls(Number(this.curEl.dataset.id))

      if (!this.curEl.classList.contains('calendar-table_inactive')) {
        this.curEl.classList.add('calendar-table_active')
        this.$emit('showEventModal', { el: this.curEl, time: this.timeCode, isShowEventModal: true })
      }
    },

    isRelevantDate (d = new Date()) {
      return d.getFullYear() === this.curYear && d.getMonth() === this.curMonth
    },

    showSelectedEvent () {
      this.$refs.cell.find((el) => {
        if (this.getTimeMls(el.dataset.date) === this.selectedEventTime) {
          this.showEvenModal(null, el)
        }
      })
    }

  }
}
