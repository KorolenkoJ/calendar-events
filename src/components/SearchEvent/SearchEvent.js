export default {
  props: {
    eventsList: Array,
    resetSearchRes: Function
  },

  data: () => ({
    searchEventVal: ''
  }),

  computed: {
    isNoRes () {
      return this.searchEventVal.length > 0 && this.filtetedEventsList.length === 0
    },

    filtetedEventsList () {
      return this.eventsList.filter((e) => {
        return this.searchEventVal &&
          e.name.toUpperCase().indexOf(this.searchEventVal.trim().toUpperCase()) !== -1
      })
    }
  },

  methods: {
    getFormattedDate (time) {
      const date = new Date(time)
      let dd = date.getDate()
      let mm = date.getMonth() + 1
      const yyyy = date.getFullYear()

      if (dd < 10) {
        dd = '0' + dd
      }
      if (mm < 10) {
        mm = '0' + mm
      }

      return `${dd} / ${mm} / ${yyyy}`
    },

    goToEvent (item) {
      this.searchEventVal = ''
      this.$emit('eventListId', item)
    },

    closeSearch () {
      this.searchEventVal = ''
    }
  }
}
