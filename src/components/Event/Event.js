export default {
  props: {
    curEl: HTMLTableCellElement,
    showHide: Function,
    time: Number
  },

  data: () => ({
    eventContainer: null,
    eventName: '',
    eventMembers: '',
    eventDesc: '',
    events: [],
    isEventExists: false
  }),

  created () {
    document.addEventListener('keydown', (event) => {
      if (event.code === 'Escape') this.showHide(false)
    })

    this.events = JSON.parse(localStorage.getItem('events'))

    if (this.events) {
      this.events.find((item) => {
        if (item.time === this.time) {
          this.isEventExists = true
          this.eventName = item.name
          this.eventMembers = item.members.join(', ')
          this.eventDesc = item.desc
        }
      })
    }
  },

  mounted () {
    this.getEventContainerPosition()
  },

  computed: {
    getMembersToArr () {
      if (this.eventMembers) {
        const arr = this.eventMembers.split(',')

        return arr.map(item => {
          return item.trim()
        }).filter(item => {
          return item.length > 0
        })
      } else {
        return []
      }
    },

    getEventYear () {
      return new Date(this.time).getFullYear()
    },

    getEventMonth () {
      return (new Date(this.time).getMonth() + 1) < 10 ? `0${new Date(this.time).getMonth() + 1}` : new Date(this.time).getMonth() + 1
    },

    getEventDay () {
      return new Date(this.time).getDate() < 10 ? `0${new Date(this.time).getDate()}` : new Date(this.time).getDate()
    }

  },

  methods: {
    setEvent (isNew) {
      if (this.eventName.length) {
        const newEvent =
                    {
                      time: this.time,
                      name: this.eventName,
                      members: this.getMembersToArr,
                      desc: this.eventDesc
                    }

        this.$emit('setEvent', newEvent, isNew)
      }

      this.showHide(false)
    },
    removeEvent () {
      const id = this.events.findIndex((item) => {
        return item.time === this.time
      })
      this.$emit('removeEvent', id)
      this.showHide(false)
    },

    getEventContainerPosition () {
      this.eventContainer = this.$refs.eventContainer

      let leftPosition = this.curEl.getBoundingClientRect().left + this.curEl.offsetWidth + 17
      let topPosition = this.curEl.getBoundingClientRect().top - 10

      if (document.documentElement.clientWidth <= (leftPosition + this.eventContainer.offsetWidth)) {
        leftPosition = this.curEl.getBoundingClientRect().left - this.eventContainer.offsetWidth - 17
        this.eventContainer.classList.add('event__content_lp')
      }

      if (document.documentElement.clientHeight <= (topPosition + this.eventContainer.offsetHeight)) {
        topPosition = this.curEl.getBoundingClientRect().bottom - this.eventContainer.offsetHeight - 10
        this.eventContainer.classList.add('event__content_bp')
      }

      this.eventContainer.setAttribute('style', `top: ${topPosition}px; left: ${leftPosition}px`)
    }
  }

}
