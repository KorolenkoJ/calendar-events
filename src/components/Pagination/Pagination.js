export default {
  props: {
    curMonth: Number,
    curYear: Number,
    setNewMonth: Function
  },

  data: () => ({
    monthes: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
  }),

  computed: {
    getCurMonthName () {
      return this.monthes[this.curMonth]
    }
  }
}
