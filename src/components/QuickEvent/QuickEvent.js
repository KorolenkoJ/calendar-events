export default {

  data: () => ({
    eventField: '',
    validError: false,
    isShow: false
  }),

  computed: {
    getQuickEventsArr () {
      const arr = this.eventField.split(',')

      return arr.map(item => {
        return item.trim()
      })
    }
  },

  methods: {
    saveQuickEvent () {
      const [time, name = 'Без темы', desc = ''] = this.getQuickEventsArr
      const newEvent =
                {
                  time: Date.parse(`${time}T00:00:00`),
                  name: name,
                  members: [],
                  desc: desc
                }

      if (Date.parse(time) && time.split('-').length === 3) {
        this.$emit('setEvent', newEvent, true)
        this.eventField = ''
        this.isShow = false
      } else {
        this.validError = true
      }
    },

    resetForm () {
      this.validError = false
      this.eventField = ''
    }

  }
}
